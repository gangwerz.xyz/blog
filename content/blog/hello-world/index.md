---
title: Hello, World!
date: "2020-07-22T12:30:03.284Z"
description: "Welcome to my blog!"
---

## Welcome

I have been considering setting up a blog for a while now, and after hearing about [Gatsby](https://www.gatsbyjs.org/) on the [Syntax](https://syntax.fm/) Podcast, it felt like the chance to pull the trigger.

## Design

My biggest constraint for selecting a framework, is hosting. Since the content of this blog will change infrequently, running a server would be overkill. Since Gatsby is a static-site generator, I could use Gitlab-CI to build and deploy the site, similar to how my [portfolio site](http://gangwerz.xyz) is already set up. Additionally, since Gatsby is built with React and GraphQL, it gives me a familiar backbone with React, and gives me the opprotunity to use GraphQL. Once I commited to Gatsby, I found a [blog starter](https://github.com/gatsbyjs/gatsby-starter-blog), and was off the races. The last point of consideration was hosting. Since I am already using an AWS S3 bucket to host my portolio, so I went with Gitlab Pages for this blog.

## Looking Forward

I have no firm plan for this blog. So, you should expect the content to have very little, if any, cosistency in terms of subject or tone. However, I will likely write a lot of reactions to new tech developments, software releases, and miscellaneous nerd culture. As for the technical side, I hope to add tags and search to the blog, while keeping the minimalist look.
